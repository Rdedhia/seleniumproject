package inheripckg;

import org.testng.annotations.Test;

import org.testng.annotations.Parameters;

import org.openqa.selenium.firefox.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.AfterClass;

public class ParentClass {
	public WebDriver Driver;
 
  @BeforeClass
  
  @Parameters("browser")

  public void beforeTest(@Optional("firefox") String browser) throws Exception
  {
  if(browser.equalsIgnoreCase("chrome"))
  {
  System.setProperty("webdriver.chrome.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\chromedriver.exe");
  ChromeOptions option=new ChromeOptions();
  option.addArguments("----disable-notification----");
  Driver=new ChromeDriver(option);
  Driver.manage().deleteAllCookies();
  Driver.manage().window().maximize();
   }
 

  else if(browser.equalsIgnoreCase("firefox"))
  {
	System.setProperty("webdriver.gecko.driver","C:\\Users\\Dell\\Desktop\\Selenium Webdriver\\geckodriver.exe");
FirefoxOptions option=new FirefoxOptions();
option.addArguments("----disable-notification----");
Driver=new FirefoxDriver(option);
Driver.manage().deleteAllCookies();
Driver.manage().window().maximize();
}
  else
  {
  throw new Exception("Browser is not correct");
  }

  Driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

  }

 
		
		
  @Test(priority=0)
  public void login() throws InterruptedException {
	 // Driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  Driver.get("https://www.shopclues.com/");
		Thread.sleep(5000);
		Driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[2]")).click();
		Driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(5000);
	  Driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("123Man");
	  Driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("riddhi17dedhia@gmail.com");
	  Driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(5000);
	  Driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
	  Thread.sleep(5000);
  }
	  @AfterClass
  public void afterClass() {
  }

}
