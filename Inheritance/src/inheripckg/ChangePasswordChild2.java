package inheripckg;

import org.testng.annotations.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ChangePasswordChild2 extends EditProfileChildClass1 {
 
  @BeforeTest
  public void beforeTest() {
  }

  @Test(priority=9)
  public void EmptyChangePassword() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[3]/div/div/div/div[1]/ul/li[4]/a")).click();
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(4000);
	  
	  String actual_message=Driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
	  String expect_message="Current password cannot be empty";
	  Assert.assertEquals(actual_message, expect_message);
	  
	 
  }
  @Test(priority=10)
  public void PasswordValid() throws InterruptedException {
	 
	  Driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("123Man");
	  Driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("123456789");
	  Driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("12345678");
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(4000);

	  String actual_message=Driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
	  String expect_message="Password must be alphanumeric";
	  Assert.assertEquals(actual_message, expect_message);
	  
  }
  @Test(priority=11)
  public void WrongPasswordValid() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
	  Driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("111Man");
	  Driver.findElement(By.xpath("//*[@id=\"password2\"]")).clear();
	  Driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("12345678");
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  Thread.sleep(4000);

	  String actual_message=Driver.findElement(By.xpath("//*[@id=\"newEqualToConfirm\"]")).getText();
	  String expect_message="New password and confirm new password do not match";
	  Assert.assertEquals(actual_message, expect_message);
	  
  }
  @Test(priority=12)
  public void ChangePassword() {
	  Driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("111Man");
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
  }
  @AfterTest
  public void afterTest() {
  }

}
