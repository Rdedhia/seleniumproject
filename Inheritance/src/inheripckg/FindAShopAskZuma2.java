package inheripckg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;

public class FindAShopAskZuma2 extends AskZumaChildDropdown {
 
  @BeforeTest
  public void beforeTest() {
  }
  @Test(priority=5)
  public void findashop() throws InterruptedException {
	  Driver.navigate().refresh();
	  Driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul/li[2]/a")).click();
	  Driver.findElement(By.xpath("//*[@id=\"tbLocation\"]")).sendKeys("Gujarat");
	  Driver.findElement(By.xpath("//*[@id=\"btnSearchShops\"]")).click();
	  
	 Thread.sleep(5000);
	 String message=Driver.findElement(By.xpath("/html/body/div[6]/div/div/div/div/div/div/div[2]")).getText();
	  System.out.println(message);
  }
  @AfterTest
  public void afterTest() {
  }

}
