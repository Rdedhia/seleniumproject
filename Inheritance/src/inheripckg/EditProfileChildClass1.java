package inheripckg;

import org.testng.annotations.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class EditProfileChildClass1 extends ParentClass {
	
  @BeforeTest
  public void beforeTest() {
	  
  }
  @Test(priority=1)
  public void MyProfileVisit() throws InterruptedException {
  Actions action=new Actions(Driver);
  WebElement hover=Driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
  action.moveToElement(hover).perform();
  Driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
	 Thread.sleep(5000); 

  }

  @Test(priority=2)
  public void FirstBlankField() throws InterruptedException {
	 Driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	 Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	 
	 String actual_message=Driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
	 String expect_message="First name cannot be blank";
	 Assert.assertEquals(actual_message, expect_message);
	 Thread.sleep(5000); 

  }
  @Test(priority=3)
  public void LastNameBlank() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Riddhi");
	  Driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1=Driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]/span")).getText();
	  String expect_message1="Last name cannot be blank";
	  Assert.assertEquals(actual_message1, expect_message1);
	  Thread.sleep(5000); 

  }

  @Test(priority=4)
  public void MobileNumberBlank() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("ABC");
	  Driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();	  
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1=Driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
	  String expect_message1="Mobile number cannot be blank";
	  Assert.assertEquals(actual_message1, expect_message1);
	  Thread.sleep(5000);
  }
  
  @Test(priority=5)
  public void InvalidFirstNameFormat() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	  Driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("111");
	  Driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("9104068661");	  
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1=Driver.findElement(By.xpath("//*[@id=\"fname\"]/span")).getText();
	  String expect_message1="Enter correct first name";
	  Assert.assertEquals(actual_message1, expect_message1);
	  Thread.sleep(5000);

  }
  @Test(priority=6)
  public void InvalidLastNameFormat() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
	  Driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Riddhi");
	  Driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
	  Driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("111");
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1=Driver.findElement(By.xpath("//*[@id=\"lname\"]/span")).getText();
	  String expect_message1="Enter correct last name";
	  Assert.assertEquals(actual_message1, expect_message1);
	  Thread.sleep(5000);
  }
  
  @Test(priority=7)
  public void InvalidMobileNumberFormat() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();

	  Driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("Abc");
	  Driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();  
	  Driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("111");	  
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1=Driver.findElement(By.xpath("//*[@id=\"mnumber\"]/span")).getText();
	  String expect_message1="Enter correct mobile number";
	  Assert.assertEquals(actual_message1, expect_message1);
  }
  @Test(priority=8)
  public void SuccessMessage() throws InterruptedException {
	  Driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();  
	  Driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("8200591540");	  
	  Driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
	  
	  String actual_message1=Driver.findElement(By.xpath("//*[@id=\"notification_85214fdf7d646b0d64eeb251383996d0\"]/div/div[2]")).getText();
	  String expect_message1="Profile has been updated successfully.";
	  Assert.assertEquals(actual_message1, expect_message1);
  }

  @AfterTest
  public void afterTest() {
  }

}
