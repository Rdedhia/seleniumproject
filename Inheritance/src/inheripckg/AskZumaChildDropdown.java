package inheripckg;

import org.testng.annotations.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class AskZumaChildDropdown extends AskzumaInheri {

  @BeforeTest
  public void beforeTest() {
  }
  @Test(priority=1)
  public void Dropdown() throws InterruptedException {
	  JavascriptExecutor js = (JavascriptExecutor)Driver;
			js.executeScript("scrollBy(0, 300)");
			Thread.sleep(2000);
			Thread.sleep(2000);

	  Driver.findElement(By.id("s2id_vehicleYearDD")).click();
	  Actions action=new Actions(Driver);
		WebElement mouse=Driver.findElement(By.xpath("//*[@id=\"select2-results-1\"]/li[4]"));
		action.moveToElement(mouse).perform();
		Driver.findElement(By.xpath("//*[@id=\"select2-results-1\"]/li[4]")).click();
		Thread.sleep(5000);
  }
  @Test(priority=2)
  public void Dropdown2() throws InterruptedException {
		Driver.findElement(By.xpath("//*[@id=\"s2id_makeDD\"]")).click();
		Thread.sleep(2000);
		 Actions action=new Actions(Driver);
			WebElement mouse=Driver.findElement(By.xpath("//*[@id=\"select2-results-46\"]/li[4]"));
			action.moveToElement(mouse).perform();
			Driver.findElement(By.xpath("//*[@id=\"select2-results-46\"]/li[4]")).click();
			Thread.sleep(5000);
			
			
	  }
  @Test(priority=3)
  public void Dropdown3() throws InterruptedException {
		Driver.findElement(By.xpath("//*[@id=\"s2id_modelDD\"]")).click();
		Thread.sleep(2000);
		 Actions action=new Actions(Driver);
			WebElement mouse=Driver.findElement(By.xpath("//*[@id=\"select2-results-90\"]/li[2]"));
			action.moveToElement(mouse).perform();
			Driver.findElement(By.xpath("//*[@id=\"select2-results-90\"]/li[2]")).click();
				
	  }
  @Test(priority=4)
  public void Dropdown4() throws InterruptedException {
	 
	  Driver.findElement(By.xpath("//*[@id=\"estimate_select\"]/div[4]/i")).click();
	  Driver.findElement(By.xpath("//*[@id=\"location\"]")).sendKeys("Ahmedabad");
	  Driver.findElement(By.xpath("//*[@id=\"icnSearchLocation\"]")).click();
	  Thread.sleep(5000);
	  Driver.findElement(By.xpath("//*[@id=\"btnMapDone\"]")).click();
	  Driver.findElement(By.id("selectJob")).click();
		Thread.sleep(1500);
		
		JavascriptExecutor js = (JavascriptExecutor)Driver;
		js.executeScript("scrollBy(0,0)");
		Thread.sleep(5000);
		String message = Driver.findElement(By.xpath("//*[@id=\"estimate_results\"]/div[4]")).getText();
		Thread.sleep(5000);
		System.out.println(message);
		

		
	  }
  @AfterTest
  public void afterTest() {
  }

}
